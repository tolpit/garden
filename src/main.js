import Vue      from 'vue'
import App      from './App'
import router   from './routes';
import SW       from './core/sw';

import firebase from 'firebase/app';

firebase.initializeApp({
    apiKey: "AIzaSyCxVzjQNS9NWf04VPKXaGg9A8u6-KkRL50",
    authDomain: "garden-fdbe1.firebaseapp.com",
    databaseURL: "https://garden-fdbe1.firebaseio.com",
    storageBucket: "garden-fdbe1.appspot.com",
    messagingSenderId: "142190003870"
});

/* eslint-disable no-new */
new Vue({
    el: '#app',
    render: h => h(App),
    router
});
