import Vue from 'vue'
import Vuex from 'vuex'

import auth from 'firebase/auth';
import { flatPlant } from './plantSchema';
import { getAllPlants, getPlant, createPlant, updatePlant, removePlant, addToGarden, getGarden, removeFromGarden, subscribe, unsubscribe } from './api';

Vue.use(Vuex);

const GardenStore = new Vuex.Store({

    state: {
        items:  [],
        active: flatPlant(),
        listType: "plants"
    },

    actions: {
        getAll ({commit, state}) {
            if(state.items.length != 0) return;

            getAllPlants()
                .then((data) => {
                    commit('setPlants', data);
                });
        },

        getOne({commit}, id) {
            getPlant(id)
                .then(data => commit('getPlant', data));
        },

        createPlant({commit}) {
            createPlant()
                .then((plant) => commit('addPlant', plant));
        },

        updatePlant({commit}, plant) {
            updatePlant(plant)
                .then(() => commit('updatePlant', plant));
        },

        removePlant({commit}, plant) {
            removePlant(plant.id)
                .then(() => commit('removePlant', plant));
        },

        getGarden({commit}) {
            getGarden()
                .then(data => commit('setPlants', data));
        },

        addToGarden({commit}, plant) {
            addToGarden(plant)
                .then(updatedPlant => commit('updatePlant', updatedPlant));
        },

        removeFromGarden({commit}, plant) {
            removeFromGarden(plant)
                .then(updatedPlant => commit('updatePlant', updatedPlant));
        },

        setType({commit, state}, type) {
            state.listType = type;
        },

        subscribe({commit, state}, res) {
            console.log(res);
            subscribe(res.id, res.subscription);
        },

        unsubscribe({commit, state}, id) {
            unsubscribe(id);
        }
    },

    mutations: {

        addPlant (state, item) {
            state.items.push(item);
        },

        setPlants(state, items) {
            Vue.set(state, 'items', items);
        },

        getPlant(state, item) {
            if(item) {
                Vue.set(state, 'active', item);
            }
        },

        updatePlant(state, plant) {
            state.items.forEach((item, index) => {
                if(item.id == plant.id) {
                    Vue.set(state.items, index, plant);
                }
            });

            if(state.active.id == plant.id) {
                state.active = Object.assign({}, state.active, plant);
            }
        },

        removePlant(state, item) {
            let index = state.items.findIndex((plant) => {
                return plant.id == item.id;
            });

            state.items.splice(index, 1);
        }
    },

    getters: {

        plants(state) {
            if(state.listType == "garden") {
                return state.items.filter(plant => plant.users && auth().currentUser && plant.users[auth().currentUser.uid]);
            }
            else {
                return state.items;
            }
        },

        plant(state) {
            return state.active;
        }

    }

});

export default GardenStore
