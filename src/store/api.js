import auth         from 'firebase/auth';
import database     from 'firebase/database';
import { PlantSchema, newPlant } from './plantSchema';

const inBrowser = typeof window !== 'undefined';

function formatPlants(snapshot) {
    let res = [];

    snapshot.forEach((plant) => {
        res.push(Object.assign(plant.val(), { id: plant.key }))
    });

    return res;
}

export function getAllPlants() {
    return new Promise((resolve, reject) => {
        database().ref('plants').once('value', (snapshot) => {
            return resolve(formatPlants(snapshot));
        });
    });
}

export function getPlant(id) {
    return new Promise((resolve, reject) => {
        database().ref(`plants/${id}`).once('value', (snapshot) => {
            return resolve(Object.assign(snapshot.val(), { id: snapshot.key }));
        });
    });
}

export function getGarden() {
    return new Promise((resolve, reject) => {
        getAllPlants()
            .then((plants) => {
                let user = auth().currentUser;

                if(!user) {
                    auth().onAuthStateChanged((user) => {
                        if(user) {
                            return resolve(plants.filter(plant => plant.users && plant.users[user.uid]));
                        } else {
                            return resolve([]);
                        }
                    });
                }
                else {
                    return resolve(plants.filter(plant => plant.users && plant.users[user.uid]));
                }
            });
    });
}

export function createPlant() {
    return new Promise((resolve, reject) => {
        const id    = database().ref('plants').push().key;
        const plant = newPlant();

        console.log(plant);

        database().ref(`plants/${id}`).set(plant)
            .then(() => {
                return Object.assign(plant, { id });
            })
            .then(resolve)
            .catch(reject);
    });
}

export function updatePlant(plant) {
    return new Promise((resolve, reject) => {
        plant.updated_at = new Date().toISOString();

        database().ref(`plants/${plant.id}/`).update(plant).then(resolve(plant));
    });
}

export function removePlant(id) {
    return new Promise((resolve, reject) => {
        database().ref(`plants/${id}`).remove().then(resolve);
    });
}

export function addToGarden(plant) {
    return new Promise((resolve, reject) => {
        const user = auth().currentUser;

        if(!plant.users) plant.users = {};

        plant.users[user.uid] = true;

        updatePlant(plant).then(resolve);
    });
}

export function subscribe(plantID, subscription) {
    return new Promise((resolve, reject) => {
        const user = auth().currentUser;

        database().ref(`users`).orderByChild("uid").equalTo(user.uid).once('value', (snapshot) => {
            const userData = snapshot.val();
            const id       = Object.keys(userData)[0];
            const notifications = userData[id].notifications || {};

            notifications[plantID] = subscription;

            database().ref(`users/${id}`).update({ notifications })
                .then(resolve);
        });
    });
}

export function unsubscribe(plantID) {
    return new Promise((resolve, reject) => {
        const user = auth().currentUser;

        database().ref(`users`).orderByChild("uid").equalTo(user.uid).once('value', (snapshot) => {
            const userData = snapshot.val();
            const id       = Object.keys(userData)[0];
            const notifications = userData[id].notifications || {};

            delete notifications[plantID];

            database().ref(`users/${id}`).update({ notifications })
                .then(resolve);
        });
    });
}

export function isSubscribed(plantID) {
    const user  = auth().currentUser;
    let resolver = null;

    if(!user) {
        auth().onAuthStateChanged((user) => {
            if(user) {
                checkNotifications(user);
            } else {
                return resolver(false);
            }
        });
    }
    else {
        checkNotifications(user);
    }

    function checkNotifications(user) {
        database().ref(`users`).orderByChild("uid").equalTo(user.uid).once('value', (snapshot) => {
            const userData = snapshot.val();
            const id       = Object.keys(userData)[0];

            return resolver((userData[id].notifications ? userData[id].notifications[plantID] : false));
        });
    }

    return new Promise((resolve) => {
        resolver = resolve;
    });
}

export function removeFromGarden(plant) {
    return new Promise((resolve, reject) => {
        const user = auth().currentUser;

        if(plant.users[user.uid]) delete plant.users[user.uid];

        database().ref(`users`).orderByChild("uid").equalTo(user.uid).once('value', (snapshot) => {
            const userData = snapshot.val();
            const id       = Object.keys(userData)[0];
            const notifications = userData[id].notifications || {};

            delete notifications[plant.id];

            database().ref(`users/${id}`).update({ notifications })
                .then(() => {
                    updatePlant(plant).then(resolve);
                });
        });
    });
}
