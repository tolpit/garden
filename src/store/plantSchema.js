export const PlantSchema = {
    name:       null,
    image:      null,
    description:null,
    arrosage:   null,
    arrosage_informations: null,
    famille:    null,
    hauteur:    null,
    exposition: null,
    sol:        null,
    feuillage:  null,
    scrap_url:  null,
    determinant:null,
    users:      {},
    updated_at: {
        type: Date,
        default: new Date().toISOString(),
    },
    created_at: {
        type: Date,
        default: new Date().toISOString(),
    }
};

export function flatPlant() {
    let res = {};

    for(let key in PlantSchema) {
        res[key] = (typeof PlantSchema[key] == 'object' && PlantSchema[key] != null ? (PlantSchema[key]['default'] ? PlantSchema[key]['default'] : {}) : '');
    }

    return res;
}

export function newPlant() {
    let res = {};

    for(let key in PlantSchema) {
        if(typeof PlantSchema[key] == 'object') {
            if(PlantSchema[key]['default']) {
                res[key] = PlantSchema[key]['default'];
            }
            else {
                res[key] = new PlantSchema[key]['type'];
            }
        }
        else {
            res[key] = '';
        }
    }

    return res;
}
