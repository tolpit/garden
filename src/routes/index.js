import Vue    from 'vue';
import Router from 'vue-router';
import auth   from 'firebase/auth';

Vue.use(Router);

import HomeView      from '../components/views/HomeView.vue';
import ListView      from '../components/views/ListView.vue';
import PlantView     from '../components/views/PlantView.vue';
import EditPlantView from '../components/views/EditPlantView.vue';
import AuthView      from '../components/views/AuthView.vue';

const routes = [
    { path: '/', component: HomeView, name: 'home' },
    { path: '/plants', component: ListView },
    { path: '/plants-edit', component: ListView, meta: { requiresAuth: true } },
    { path: '/plant/:id', component: PlantView },
    { path: '/plant/edit/:id', component: EditPlantView, meta: { requiresAuth: true } },
    { path: '/signup', component: AuthView, meta: { noForAuth: true }  },
    { path: '/login',  component: AuthView, meta: { noForAuth: true } }
];

const router = new Router({
    mode: 'history',
    scrollBehavior: () => ({ y: 0 }),
    routes
});

router.beforeEach((to, from, next) => {
    if (to.matched.some(record => record.meta.requiresAuth)) {
        // this route requires auth, check if logged in
        // if not, redirect to login page.
        if (auth().currentUser) {
            next()
        } else {
            auth().onAuthStateChanged(function(user) {
                if (user) {
                    next();
                } else {
                    next({
                        path: '/',
                        query: { redirect: to.fullPath }
                    });
                }
            });
        }
    }
    else if(to.matched.some(record => record.meta.noForAuth)) {
        if(auth().currentUser) {
            next({
                path: '/',
                query: { redirect: to.fullPath }
            })
        }
        else {
            auth().onAuthStateChanged(function(user) {
                if(!user) {
                    next();
                } else {
                    next({
                        path: '/',
                        query: { redirect: to.fullPath }
                    });
                }
            });
        }
    }
    else {
        next(); // make sure to always call next()!
    }
});

export default router;
