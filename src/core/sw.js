class SW {

    constructor() {
        this.can = 'serviceWorker' in navigator;
        this.serviceWorkerRegistration = null;

        this.hire('/service-worker.js');
        //this.setup('dev', true);
        //this.setup('onLine', false);
    }

    hire(name) {
        if(!this.can) return;

        navigator.serviceWorker
            .register(name)
            .then((serviceWorkerRegistration) => {
                this.serviceWorkerRegistration = serviceWorkerRegistration;
            })
            .catch(function(err) {
                console.error(err);
            });
    }

    cmd(data) {
        if(!this.can) return;

        return new Promise(function(resolve, reject) {
            var messageChannel = new MessageChannel();

            messageChannel.port1.onmessage = function(event) {
                if (event.data.error) {
                    reject(event.data.error);
                } else {
                    resolve(event.data);
                }
            };
            // This sends the message data as well as transferring messageChannel.port2 to the service worker.
            // The service worker can then use the transferred port to reply via postMessage(), which
            // will in turn trigger the onmessage handler on messageChannel.port1.
            // See https://html.spec.whatwg.org/multipage/workers.html#dom-worker-postmessage
            if(navigator.serviceWorker.controller)
                navigator.serviceWorker.controller.postMessage(data, [messageChannel.port2]);
        });
    }

    message(data) {
        return new Promise(function(resolve, reject) {
            var messageChannel = new MessageChannel();

            messageChannel.port1.onmessage = function(event) {
                if (event.data.error) {
                    reject(event.data.error);
                } else {
                    resolve(event.data);
                }
            };
            // This sends the message data as well as transferring messageChannel.port2 to the service worker.
            // The service worker can then use the transferred port to reply via postMessage(), which
            // will in turn trigger the onmessage handler on messageChannel.port1.
            // See https://html.spec.whatwg.org/multipage/workers.html#dom-worker-postmessage
            if(navigator.serviceWorker.controller)
                navigator.serviceWorker.controller.postMessage(data, [messageChannel.port2]);
        });
    }

    subscribe() {
        return new Promise((resolve, reject) => {
            if (!('showNotification' in ServiceWorkerRegistration.prototype)) {
                console.warn('Notifications aren\'t supported.');
                return reject();
            }

            if(this.serviceWorkerRegistration) {
                doSubscribtion(this.serviceWorkerRegistration)
            }
            else {
                navigator.serviceWorker.ready.then(doSubscribtion);
            }

            function doSubscribtion(serviceWorkerRegistration) {
                serviceWorkerRegistration.pushManager.subscribe({userVisibleOnly: true})
                    .then((subscription) => {
                        return resolve(subscription);
                    })
                    .catch((e) => {
                        if (Notification.permission === 'denied') {
                            console.warn('Permission for Notifications was denied');
                            reject();
                        } else {
                            console.error('Unable to subscribe to push.', e);
                            reject();
                        }
                    });
            }
        });
    }

    unsubscribe() {
        return new Promise((resolve, reject) => {
            if(this.serviceWorkerRegistration) {
                doUnsubscribtion(this.serviceWorkerRegistration)
            }
            else {
                navigator.serviceWorker.ready.then(doUnsubscribtion);
            }

            function doUnsubscribtion(serviceWorkerRegistration) {
                serviceWorkerRegistration.pushManager.getSubscription().then(
                    function (pushSubscription) {
                        // Check we have a subscription to unsubscribe
                        if (!pushSubscription) return resolve({done: true});

                        //Unsubscribe the user
                        pushSubscription.unsubscribe().then(function (successful) {
                            resolve({done: successful});
                        }).catch(function (e) {
                            // We failed to unsubscribe
                            console.log('Unsubscription error: ', e);

                            reject();
                        });
                    });
            }
        });
    }

    getSubscribtion() {
        return new Promise((resolve, reject) => {
            this.serviceWorkerRegistration.pushManager.getSubscription()
                .then((subscription) => {
                    if(!subscription) {
                        this.subscribe().then((subscription) => {
                            resolve(subscription);
                        });
                    }
                    else {
                        resolve(subscription);
                    }
                });
        });

    }

    setup(name, value) {
        this.cmd({
            cmd: "setup",
            name,
            value
        });
    }

    has(url, callback) {
        this.cmd({
            cmd: "has",
            url: url
        }).then(callback);
    }

    add(url, callback) {
        this.cmd({
            cmd: "add",
            url: url
        }).then(callback);
    }

    delete(url, callback) {
        this.cmd({
            cmd: "delete",
            url: url
        }).then(callback);
    }

}

export default new SW();
