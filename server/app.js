const cron       = require('node-cron');
const firebase   = require('firebase');
const moment     = require('moment');
const webpush    = require('web-push');

firebase.initializeApp({
    apiKey: "AIzaSyCxVzjQNS9NWf04VPKXaGg9A8u6-KkRL50",
    authDomain: "garden-fdbe1.firebaseapp.com",
    databaseURL: "https://garden-fdbe1.firebaseio.com",
    storageBucket: "garden-fdbe1.appspot.com",
    messagingSenderId: "142190003870"
});

const database = firebase.database();

firebase.auth().signInWithEmailAndPassword("baes.theo@gmail.com", "foobar");

webpush.setGCMAPIKey('AIzaSyDVDDZPxP8t0LMrCrHXJhnzhB9fkGvRyPg');

const now = new Date();

function getPlants() {
    return new Promise((resolve, reject) => {
        database.ref('plants').once('value', function(snapshot) {
            var res = [];

            snapshot.forEach((plant) => {
                res.push(Object.assign(plant.val(), { id: plant.key }));
            });

            return resolve(res.filter((plant) => {
                return (plant.arrosage && plant.users);
            }));
        });
    });
}

function canPlantSendNotification(plants) {
    return new Promise((resolve, reject) => {
        var res = plants.filter((plant) => {
            if(!plant.next_notification) {
                return true;
            }
            else {
                return moment(now).isAfter(plant.next_notification);
            }
        });

        if(!res.length) return reject(new Error("Aucune plante à arroser"));
        else return resolve(res);
    });
}

function getUsers(plants) {
    return new Promise((resolve, reject) => {
        database.ref('users').once('value', function(snapshot) {
            var users = [];

            snapshot.forEach((user) => {
                users.push(user.val());
            });

            return resolve({ plants, users });
        });
    });
}

function setUsers(data) {
    return new Promise((resolve, reject) => {
        return resolve(data.plants.map((plant) => {
            for(var userUID in plant.users) {
                data.users.forEach((user) => {
                    for(var plantID in user.notifications) {
                        if(user.uid == userUID && plantID == plant.id) {
                            plant.users[userUID] = user.notifications[plantID];
                        }
                    }
                })
            }

            return plant;
        }));
    });
}

function sendNotification(plants) {
    return new Promise((resolve, reject) => {
        plants.forEach((plant) => {
            const payload = `N'oublier pas d'arroser ${plant.determinant + (plant.determinant != "l'" ? " " : "")} ${plant.name} aujourd'hui ;)`;

            for(var userID in plant.users) {
                webpush.sendNotification(plant.users[userID], payload)
                    .then((res) => {
                        console.log(res);
                    })
                    .catch(err => console.error(err));
            }
        });

        return resolve(plants);
    });
}

function updatePlants(plants) {
    var updates = {};

    plants.forEach((plant) => {
        var next_notification = moment(now).add(1, 'days');

        switch(plant.arrosage) {
            case '1':
            case '2':
            case '3':
                next_notification = moment(now).add(parseInt(plant.arrosage, 10), 'days');
                break;

            case '4':
                next_notification = moment(now).add(1, 'weeks');
                break;

            case '5':
                next_notification = moment(now).add(2, 'weeks');
                break;

            case '6':
                next_notification = moment(now).add(1, 'months');
                break;
        }

        next_notification = next_notification.format();

        updates[`plants/${plant.id}/next_notification`] = next_notification;
    });

    database.ref().update(updates, function(err) {
        if(err) console.error(err);
    });
}

getPlants()
    .then(canPlantSendNotification)
    .then(getUsers)
    .then(setUsers)
    .then(sendNotification)
    .then(updatePlants)
    .catch((err) => {
        console.error(err);
        process.exitCode = 1;
        process.exit();
    });
