// [Working example](/serviceworker-cookbook/push-subscription-management/).

// Listen to `push` notification event. Define the text to be displayed
// and show the notification.
self.addEventListener('push', function(event) {
    var payload = event.data.text();

    event.waitUntil(self.registration.showNotification('Your Garden', {
        body: payload,
        icon: '/static/img/icon.png'
    }));
});

// Listen to  `pushsubscriptionchange` event which is fired when
// subscription expires. Subscribe again and register the new subscription
// in the server by sending a POST request with endpoint. Real world
// application would probably use also user identification.
self.addEventListener('pushsubscriptionchange', function(event) {
    console.log('Subscription expired');
    event.waitUntil(
        self.registration.pushManager.subscribe({ userVisibleOnly: true })
            .then(function(subscription) {
                console.log('Subscribed after expiration', subscription.endpoint);
            })
    );
});
